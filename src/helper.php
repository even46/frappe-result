<?php
declare(strict_types=1);

use frappe\result\ApiResult;
use think\response\Json;

if (!function_exists('api_success')) {
    /**
     * 成功
     * @param mixed|null $data
     * @param string $msg
     * @param int $code
     * @return Json
     * @author yinxu
     * @date 2024/3/28 20:41:20
     */
    function api_success($data = null, string $msg = "ok", int $code = 200): Json
    {
        return ApiResult::success($data, $msg, $code);
    }
}

if (!function_exists('api_error')) {
    /**
     * 失败
     * @param \Throwable|string $msg
     * @param int $code
     * @return Json
     * @author yinxu
     * @date 2024/3/28 20:41:20
     */
    function api_error($msg = "fail", int $code = 0): Json
    {
        return ApiResult::error($msg, $code);
    }
}